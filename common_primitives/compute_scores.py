import os
import typing

import pandas  # type: ignore

from d3m import container, exceptions, utils as d3m_utils
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams, problem
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import construct_predictions

__all__ = ('ComputeScoresPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class ElementsHyperparams(hyperparams.Hyperparams, set_names=False):
    metric = hyperparams.Enumeration(
        values=[metric.name for metric in problem.PerformanceMetric],
        # Default is ignored.
        # TODO: Remove default. See: https://gitlab.com/datadrivendiscovery/d3m/issues/141
        default='ACCURACY',
    )
    pos_label = hyperparams.Hyperparameter[typing.Union[str, None]](None)
    k = hyperparams.Hyperparameter[typing.Union[int, None]](20)


class Hyperparams(hyperparams.Hyperparams):
    metrics = hyperparams.Set(
        elements=ElementsHyperparams,
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of metrics to compute.",
    )


# TODO: Add "can_accept".
class ComputeScoresPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive that takes a DataFrame with predictions and a scoring Dataset and computes
    scores for given metrics and outputs them as a DataFrame.

    It searches only the dataset entry point resource for target columns
    (which should be marked with ``https://metadata.datadrivendiscovery.org/types/TrueTarget``
    semantic type) in the scoring Dataset.

    Primitive does not align rows between truth DataFrame and predictions DataFrame, it
    is expected that metric code does that if necessary. Similarly, it does not align
    columns order either.

    It uses metadata to construct the truth DataFrame and renames the index column to match
    the standard names ``d3mIndex``. It encodes any float vectors as strings.

    For predictions DataFrame it expects that it is already structured correctly with correct
    column names and it leaves to metric code to validate that truth DataFrame and predictions
    DataFrame match. It does not use or expect metadata on predictions DataFrame. Predictions
    DataFrame should already have float vectors encoded as strings.
    """

    __author__ = 'Mingjie Sun <sunmj15@gmail.com>'
    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '799802fb-2e11-4ab7-9c5e-dda09eb52a70',
            'version': '0.2.0',
            'name': "Compute scores given the metrics to use",
            'python_path': 'd3m.primitives.evaluation.compute_scores.Common',
            'source': {
                'name': common_primitives.__author__,
                'contact': 'mailto:sunmj15@gmail.com',
                'uris': [
                    'https://gitlab.com/datadrivendiscovery/common-primitives/blob/master/common_primitives/compute_scores.py',
                    'https://gitlab.com/datadrivendiscovery/common-primitives.git',
                ],
            },
            'installation': [{
                'type': metadata_base.PrimitiveInstallationType.PIP,
                'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                    git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
                ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ACCURACY_SCORE,
                metadata_base.PrimitiveAlgorithmType.F1_SCORE,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.EVALUATION,
        },
    )

    def produce(self, *, inputs: Inputs, score_dataset: container.Dataset, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # type: ignore
        if not self.hyperparams['metrics']:
            raise ValueError("\"metrics\" hyper-parameter cannot be empty.")

        truth = self._get_truth(score_dataset)
        predictions = self._get_predictions(inputs)

        outputs: typing.Dict[str, typing.List] = {
            'metric': [],
            'value': [],
        }

        for metric_configuration in self.hyperparams['metrics']:
            metric = problem.PerformanceMetric[metric_configuration['metric']]
            params = {}
            for param_name, param_value in metric_configuration.items():
                if param_name == 'metric':
                    continue
                if param_value is None:
                    continue
                params[param_name] = param_value

            if metric.requires_confidence() and 'confidence' not in predictions.columns:
                raise exceptions.InvalidArgumentValueError(f"Metric {metric.name} requires confidence column in predictions, but it is not available.")

            score = metric.get_class()(**params).score(truth, predictions)

            outputs['metric'].append(metric.name)
            outputs['value'].append(score)

        # We list columns explicitly to assure the order of columns.
        results = container.DataFrame(data=outputs, columns=['metric', 'value'], generate_metadata=True)

        # Not really necessary, but it does not hurt. In theory somebody could list same metric multiple times
        # (maybe with different params), so we use "PrimaryMultiKey" here.
        results.metadata = results.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 0), 'https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey')
        results.metadata = results.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/Score')

        return base.CallResult(results)

    def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, score_dataset: container.Dataset,  # type: ignore
                      timeout: float = None, iterations: int = None) -> base.MultiCallResult:
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, score_dataset=score_dataset)

    def fit_multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, score_dataset: container.Dataset,  # type: ignore
                          timeout: float = None, iterations: int = None) -> base.MultiCallResult:
        return self._fit_multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, score_dataset=score_dataset)

    # TODO: Instead of extracting true targets only from the dataset entry point, first denormalize and then extract true targets.
    def _get_truth(self, score_dataset: container.Dataset) -> pandas.DataFrame:
        """
        Extracts true targets from the Dataset's entry point, or the only tabular resource.
        It requires that there is only one primary index column, which it makes the first
         column, named ``d3mIndex``. Then true target columns follow.

        We return a regular Pandas DataFrame with column names matching those in the metadata.
        We convert all columns to strings to match what would be loaded from ``predictions.csv`` file.
        It encodes any float vectors as strings.
        """

        main_resource_id, main_resource = base_utils.get_tabular_resource(score_dataset, None, has_hyperparameter=False)

        # We first copy before modifying in-place.
        main_resource = container.DataFrame(main_resource, copy=True)
        main_resource = construct_predictions.ConstructPredictionsPrimitive._encode_columns(main_resource)

        dataframe = self._to_dataframe(main_resource)

        indices = list(score_dataset.metadata.get_index_columns(at=(main_resource_id,)))
        targets = list(score_dataset.metadata.list_columns_with_semantic_types(['https://metadata.datadrivendiscovery.org/types/TrueTarget'], at=(main_resource_id,)))

        if not indices:
            raise exceptions.InvalidArgumentValueError("No primary index column.")
        elif len(indices) > 1:
            raise exceptions.InvalidArgumentValueError("More than one primary index column.")
        if not targets:
            raise ValueError("No true target columns.")

        dataframe = dataframe.iloc[:, indices + targets]

        dataframe = dataframe.rename({dataframe.columns[0]: 'd3mIndex'})

        if 'confidence' in dataframe.columns[1:]:
            raise ValueError("True target column cannot be named \"confidence\". It is a reserved name.")
        if 'd3mIndex' in dataframe.columns[1:]:
            raise ValueError("True target column cannot be named \"d3mIndex\". It is a reserved name.")

        if d3m_utils.has_duplicates(dataframe.columns):
            duplicate_names = list(dataframe.columns)
            for name in set(dataframe.columns):
                duplicate_names.remove(name)
            raise exceptions.InvalidArgumentValueError("True target columns have duplicate names: {duplicate_names}".format(
                duplicate_names=sorted(set(duplicate_names)),
            ))

        return dataframe

    def _get_predictions(self, inputs: Inputs) -> pandas.DataFrame:
        """
        It requires that predictions already have the right structure (one ``d3mIndex``
        column, at most one ``confidence`` column, no duplicate column names).

        We return a regular Pandas DataFrame with column names matching those in the metadata.
        We convert all columns to strings to match what would be loaded from ``predictions.csv`` file.
        Predictions DataFrame should already have float vectors encoded as strings.
        """

        dataframe = self._to_dataframe(inputs)

        if 'd3mIndex' not in dataframe.columns:
            raise exceptions.InvalidArgumentValueError("No primary index column.")

        if d3m_utils.has_duplicates(dataframe.columns):
            duplicate_names = list(dataframe.columns)
            for name in set(dataframe.columns):
                duplicate_names.remove(name)
            raise exceptions.InvalidArgumentValueError("Predicted target columns have duplicate names: {duplicate_names}".format(
                duplicate_names=sorted(set(duplicate_names)),
            ))

        return dataframe

    def _to_dataframe(self, inputs: container.DataFrame) -> pandas.DataFrame:
        # We have to copy, otherwise setting "columns" modifies original DataFrame as well.
        dataframe = pandas.DataFrame(inputs, copy=True)

        column_names = []
        for column_index in range(len(inputs.columns)):
            column_names.append(inputs.metadata.query_column(column_index).get('name', inputs.columns[column_index]))

        # Make sure column names are correct.
        dataframe.columns = column_names

        # Convert all columns to string.
        return dataframe.astype(str)
