import unittest
import os

from common_primitives import dataset_term_filter
from d3m import container, exceptions


class TermFilterPrimitiveTestCase(unittest.TestCase):
    def test_inclusive(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': True,
            'terms': ['AAA', 'CCC'],
            'match_whole': True
        })

        filter_primitive = dataset_term_filter.TermFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        self.assertTrue(set(new_dataset['learningData']['code'].unique()) == set(['AAA', 'CCC']))

    def test_exclusive(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': False,
            'terms': ['AAA', 'CCC'],
            'match_whole': True
        })

        filter_primitive = dataset_term_filter.TermFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        self.assertTrue(set(new_dataset['learningData']['code'].unique()) == set(['BBB']))

    def test_partial_no_match(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': True,
            'terms': ['AA', 'CC'],
            'match_whole': False
        })

        filter_primitive = dataset_term_filter.TermFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        self.assertTrue(set(new_dataset['learningData']['code'].unique()) == set(['AAA', 'CCC']))

    def test_escaped_regex(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 4,
            'inclusive': True,
            'terms': ['40.2'],
            'match_whole': False
        })

        filter_primitive = dataset_term_filter.TermFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        self.assertListEqual(list(new_dataset['learningData']['value']), ['40.2346487255306'])

    def test_row_metadata_removal(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataset.metadata = dataset.metadata.update(('learningData', 1), {'a': 0})
        dataset.metadata = dataset.metadata.update(('learningData', 2), {'b': 1})

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': 'learningData',
            'column': 1,
            'inclusive': False,
            'terms': ['AAA'],
            'match_whole': True
        })

        filter_primitive = dataset_term_filter.TermFilterPrimitive(hyperparams=hp)
        new_dataset = filter_primitive.produce(inputs=dataset).value

        # verify that the lenght is correct
        self.assertEquals(len(new_dataset['learningData']), new_dataset.metadata.query(('learningData',))['dimension']['length'])

        # verify that the rows were re-indexed in the metadata
        self.assertEquals(new_dataset.metadata.query(('learningData', 0))['a'], 0)
        self.assertEquals(new_dataset.metadata.query(('learningData', 1))['b'], 1)
        self.assertFalse('b' in new_dataset.metadata.query(('learningData', 2)))

    def test_bad_resource(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'resource_id': None,
            'column': 1,
            'inclusive': True,
            'terms': ['AA', 'CC'],
            'match_whole': False
        })

        filter_primitive = dataset_term_filter.TermFilterPrimitive(hyperparams=hp)
        with self.assertRaises(exceptions.InvalidArgumentValueError):
            filter_primitive.produce(inputs=dataset)

    def test_can_accept_success(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'resource_id': '0',
            'inclusive': True,
            'terms': ['AAA', 'BBB'],
            'match_whole': False
        })

        accepted_metadata = dataset_term_filter.TermFilterPrimitive.can_accept(
            method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
        )

        self.assertIsNotNone(accepted_metadata)

    def test_can_accept_bad_resource(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))
        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        filter_hyperparams_class = dataset_term_filter.TermFilterPrimitive.metadata.get_hyperparams()
        hp = filter_hyperparams_class({
            'column': 1,
            'resource_id': None,
            'inclusive': True,
            'terms': ['AAA', 'BBB'],
            'match_whole': False
        })

        with self.assertRaises(ValueError):
            dataset_term_filter.TermFilterPrimitive.can_accept(
                method_name='produce', arguments={'inputs': dataset.metadata}, hyperparams=hp
            )


if __name__ == '__main__':
    unittest.main()
